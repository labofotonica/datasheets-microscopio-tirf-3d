# Se importan paquetes
import os.path as path
import pathlib
from glob import glob
from pysuppose.addons.plots import PlotSources
import scipy.io as sio
import shutil
from os import mkdir
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import sys
from copy import copy

# Se importan paquetes relacionados a SUPPOSe
from pysuppose import SupposeRuntimeError
from pysuppose.addons.plots import PlotSources3D
from pysuppose.addons.report import ReportAddOn
from pysuppose.engine import SupposeEngine
from pysuppose.engine import after_convolve_recalculate_alpha, after_convolve_subtract_mean
from pysuppose.bases.psf import DataPSF
from pysuppose.bases.sample import Sample
from pysuppose.bases.population import Population
from pysuppose.individual_generators.alternative_generators import FitnessBasedGenerator, FitnessBasedGenerator3D
from pysuppose.engine import SupposeEngine
from pysuppose.addons.save import SaveArray


# Carga de parametros por linea de comandos.
max_nsources = int(sys.argv[7]) # Cantidad de fuentes
alpha = int(sys.argv[6]) # Alpha inicial
gpu_index = int(sys.argv[2]) # Indice de la GPU a utilizar
psf_oversample = int(sys.argv[3]) # Oversample de la PSF respecto a la ROI
iterations = int(sys.argv[4]) # Cantidad de iteraciones
initial_kicksize = float(sys.argv[8]) # Tamaño de la patada inicial
subtract_mean = sys.argv[9].lower() == 'true' # Resta de valor medio luego de convolucionar
folder_path = sys.argv[1] # Path de datos de entrada

# Seteo de parametros del algoritmo
individuals = 100 # Cantidad de individuos
elite = 10 # Cantidad de la elite

mutate_radius = 0.01 # Radio de mutacion
mutate_percent = 0.2 # Porcentaje de mutacion
cross_percent = 0.4 # Porcentaje de cruzas

# Intervalo de reporte
interval_mode = 'arange'
interval_value = 100 

psf_file_keyword = 'psf'
roi_file_keyword = 'roi'

now = datetime.now()

# Se carga archivo y se toma el nombre
filename = glob(path.join(folder_path, '*'+psf_file_keyword+'*'))[0]
OUTPUT_NAME= filename.split('/')[2]

#Quito fecha y hora
OUTPUT_NAME = OUTPUT_NAME[20:]

# Se configura la carpeta de salida
input_folder = sys.argv[5]
output_folder = path.join('output', input_folder)

# Se quita el prefijo del nombre de la carpeta
OUTPUT_PREFIX = now.strftime("%Y_%m_%d_-%H_%M_%S")
OUTPUT_FOLDER_NAME = OUTPUT_PREFIX + '-' + OUTPUT_NAME + '-mutate_radius_' + str(mutate_radius) + '-max_nsources_' + str(max_nsources) + '-alpha_' + str(alpha)
OUTPUT_FOLDER = path.join(output_folder, OUTPUT_FOLDER_NAME)
pathlib.Path(OUTPUT_FOLDER).mkdir(parents=True, exist_ok=True)

# Configuracion de paths de guardado
PSF_PATH = path.join(OUTPUT_FOLDER, OUTPUT_PREFIX + '-psf.mat')
ROI_PATH = path.join(OUTPUT_FOLDER, OUTPUT_PREFIX + '-roi.mat')
REPORT_PATH = path.join(OUTPUT_FOLDER, OUTPUT_PREFIX + '-report.txt')
FITNESS_REGISTER_PATH = path.join(OUTPUT_FOLDER, OUTPUT_PREFIX + '-fitness_register.mat')
INDIVIDUAL_PATH = path.join(OUTPUT_FOLDER, OUTPUT_PREFIX + '-individual.mat')
PYSUPPOSE_PARAMS_PATH = path.join(OUTPUT_FOLDER, OUTPUT_PREFIX + '-pysuppose_params.mat')

for filename in glob(path.join(folder_path, '*'+psf_file_keyword+'*')):
    psf_struct = sio.loadmat(filename)
    psf_struct['original_file'] = filename.split('/')[-1]
    sio.savemat(PSF_PATH, psf_struct)
for filename in glob(path.join(folder_path, '*'+roi_file_keyword+'*')):
    roi_struct = sio.loadmat(filename)
    roi_struct['original_file'] = filename.split('/')[-1]
    sio.savemat(ROI_PATH, roi_struct)
    
# Se carga muestra y PSF
psf_data = psf_struct['PSF']
roi_data = roi_struct['roi']

# Inicializacion de PSF y ROI
psf = DataPSF(data=psf_data, pixel_size=(1, 1, 1))
sigma_sample = psf_struct['sigma'] / psf_oversample
sample = Sample(data=roi_data, subtract_mean=subtract_mean, pixel_size=(psf_oversample, psf_oversample, psf_oversample))


# Se generan los individuos iniciales
generator = FitnessBasedGenerator3D(sample=sample,
                            psf=psf,
                            method="manual",
                            method_value=max_nsources,
                            fitness_function=FitnessBasedGenerator.mean_squared_error,
                            local_fitness=FitnessBasedGenerator.per_pixel_error,
                            subtract_mean=subtract_mean,
                            #alpha=alpha, Hago que calcule el alpha solo
                            allow_beta=False,
                            max_iter=max_nsources,
                            grid_length=1,
                            grid_spacing=0.1,
                            max_eval=1,
                            smooth_radius=None,
                            random_scale=0)

# Descomentar para el gráfico de los individuos iniciales
"""
individual = generator.generate_individual()
positions = individual.positions
print(positions.shape)

print('Sources = ' + str(positions.shape[0]))

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter3D(positions[:, 0], positions[:, 1], positions[:, 2])
plt.show()
"""
# Generación de la población inicial
population = generator.generate_population(size=individuals, kick_size=initial_kicksize*sigma_sample, elite=elite)

# Inicialziación y configuración del motor de SUPPOSe
engine = SupposeEngine()

# Se registran los distintos puntos de control.
if subtract_mean:
    engine.register_as_checkpoint(after_convolve_subtract_mean) # Resta de valor medio
engine.register_as_checkpoint(after_convolve_recalculate_alpha) # Recalculo de alpha


from pysuppose.bases.device import GPU
try:
    engine.resource_manager.add_device(GPU(gpu_index))
except SupposeRuntimeError:
    print("\n La GPU solicitada no esta disponible")
    exit()

# Configuracion de parametros de algogen.
engine.mutation.mutate_percent = mutate_percent
engine.mutation.mutate_radius = mutate_radius * sigma_sample
engine.crossover.cross_percent = cross_percent

# Configuracion de muestras e individuos iniciales.
engine.sample = sample
engine.psf = psf
engine.population = population

report = ReportAddOn(interval_value=interval_value, interval_mode=interval_mode)
report.setup_file(REPORT_PATH)

# Guardado
save_fitness_register = SaveArray(path=FITNESS_REGISTER_PATH, interval_value=interval_value, interval_mode=interval_mode,
                                attributes={'best_fitness': 'engine.population.best_fitness',
                                            'generation': 'engine.population.generation',
                                            'mean_fitness': 'engine.population.mean_fitness',
                                            'std_fitness': 'engine.population.std_fitness'})

save_individual = SaveArray(path=INDIVIDUAL_PATH, interval_value=interval_value, interval_mode=interval_mode,
                            attributes={'alpha': 'engine.population.best_individual.alpha', 'fitness': 'engine.population.best_fitness',
                                        'generation': 'engine.population.generation',
                                        'positions': 'engine.population.best_individual.positions'})


# Conexión de addons
engine.connect(report, save_fitness_register, save_individual)

# Ejecucion
results = engine.run(iterations)


nsources = engine.population.best_individual.positions.shape[0]

# Guardado de parametros de pySUPPOSe
sio.savemat(PYSUPPOSE_PARAMS_PATH, {'max_nsources': max_nsources, 'sources': nsources, 'elite': elite, 'individuals': individuals,
                                    'iterations': iterations, 'mutate_percent': mutate_percent,
                                    'mutate_radius': mutate_radius, 'cross_percent': cross_percent})
